resource "aws_key_pair" "ec2_keypair" {
  key_name   = var.ec2_props.public_key_name
  public_key = var.ec2_props.ssh_pubkey

  tags = local.common_tags
}

resource "aws_security_group" "ec2_egress_securitygroup" {
  name        = var.ec2_props.sg_name
  description = "Allow outbound traffic to all destinations on all ports"
  vpc_id      = var.ec2_props.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = local.common_tags
}

resource "aws_network_interface" "ec2_network_interface" {
  subnet_id   = var.ec2_props.subnet_id
  tags = local.common_tags
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.ec2_egress_securitygroup.id
  network_interface_id = aws_network_interface.ec2_network_interface.id
}

resource "aws_instance" "ec2_instance" {
  ami           = var.ec2_props.ami
  instance_type = var.ec2_props.instance_type
  key_name      = aws_key_pair.ec2_keypair.key_name

  network_interface {
    network_interface_id = aws_network_interface.ec2_network_interface.id
    device_index         = 0
  }

  root_block_device {
    volume_size = var.ec2_props.ebs_volume_size
    volume_type = var.ec2_props.ebs_volume_type
    encrypted = true
    kms_key_id = var.ec2_props.ebs_kms_key_id
    iops = var.ec2_props.ebs_iops
  }

  user_data_replace_on_change = true
  user_data = templatefile("${path.module}/scripts.cfg", {
    script = {
      name = var.ec2_props.user_data_script_name
      tailscale_auth_key = var.ts_auth_key
      instance_name = var.ec2_props.instance_name
    }
  })

  tags = merge(tomap({Name = var.ec2_props.instance_name}), local.common_tags)
}
