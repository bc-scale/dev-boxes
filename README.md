# Dev Boxes
A terraform module with custom scripts that can be used to spin up ec2 dev environments for home use. BYO VPC and Subnet from AWS. These dev boxes are def haram from an AC306 perspective.

## Requirements
**AWS Account**: If you are going to get one, go check the internet for offers; there are usually discounts for students or credits for new users. After you create an account, create an **IAM user access key** with permissions to create/edit/delete EC2 instances + security groups + network interfaces + keypairs + ebs volumes + tags. If you use s3 as a backend, you will need to grant that user access to the s3 bucket too. There are tons of guides online that walk you through creating such a user. We would link a guide here, but it would probably go rotten within a week of being posted.

**tailscale Account**: We need a reusable `tailscale` access key for `tailscale` stuff. If you do not know what `tailscale` is, read the `tailscale` section in the **Current Support/Features** section. Sign up for `tailscale` [here](https://login.tailscale.com/login) and get the reusable auth key [here](https://login.tailscale.com/admin/settings/keys) - feel free to add tags to your auth keys too. If you wish to use `tailscale` domain names or TLS, then you will need to [enable this on tailscale yourself](https://tailscale.com/kb/1153/enabling-https/); fortunately, the process is pretty simple. The TLS and DNS features offered by `tailscale` are free as well.

## Current Support/Features
Dev-boxes currently supports:
- **Tailscale**: Supports Ubuntu only. Sets up `tailscale` + auth.
- **Tailscale + Code-Server**: Ubuntu only. Sets up `tailscale` + `code-server` (+ TLS + `tailscale` domain).
- **Null**: (Vanilla EC2 Instance; i.e. No-Op): Please note that we currently set up EC2 instaces with wide open egress, but we do not create any ingress rules at all. You will need to poke holes to get into the network yourself.

### Tailscale
Scripts that use `tailscale` require a [tailscale account](https://login.tailscale.com/login). `tailscale` supports Single Sign On via Google/Microsoft/Github accoounts, or simply via a tailscale account, and has clients for most platforms. All the `tailscale` features used here are free features, so unless you want to do something crazy, you should not need to pay for anything. We may work on something in the future that does not use `tailscale`, so as to not lock all our solutions down to a single product.

`tailscale` bascially allows devices to join a private network that it sets up. Once devices are on the `tailscale` network, they can talk to each other via their `tailscale` IP. On linux machines, this is done via a virtual interface. Not sure about Windows/OSX. 

## Deploy
So all of this is great, but how do you use it?

Create a gitlab repository for yourself. Call it `dev-boxes-ec2-tf-state` or something meaningful. Add this repository as a submodule to your code:
```
git submodule add ../../ac306/dev-boxes.git ./dev-boxes
```
Make sure you commit the submodule file.

Copy the files in the `templates/` folder (including the `gitlab-ci.yml` and `.gitignore` files) and put them in your repository's root directory. A couple of things we would recommend changing:
- Change the region in `main.tf` to the region where you would like your instances to be deployed. 
- Change the `common_tags` locals in `main.tf` to whatever tags you want on your instances; you could even make them emtpy if you want.
- Change you terraform backend in `main.tf` to anything other than a local backend. Refer to the [terraform docs](https://www.terraform.io/language/settings/backends/configuration) for more information. `main.tf` currently uses a local backend, which works fine locally and for testing; but will not work with gitlab pipelines, because the pipeline runner does not commit the state file back to the repo, so subsequent runs would attempt to re-create infrastructure and you could end up owing uncle Jeff a lot of money; we would recommend using [s3 as a backend](https://www.terraform.io/language/settings/backends/s3) because you could just add it to your AWS access key's permissions (so fewer creds to juggle), but [gitlab's terraform statefile](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) seems to be a popular option too.
- (Optional) Change `instances_in_region` to a different variable name to something meaningful. This is defined in both files, so change it everywhere.

Your directory structure should look like this
```
├─ dev-boxes/
|  ├─ (...) <our repo data>
├─ .gitlab-ci.yml
├─ .gitignore
├─ instances.auto.tfvars
├─ main.tf
```

Once you have changed these files, add your instance definitions to the `instances.auto.tfvars` file to the `instances_in_regions` (which you might have changed as instructed above) list; see the **Terraform Vars** section for more information.

### Terraform Vars
```go
{
    // The name of the ec2-instance; this will be used for its hostname too, so make sure these are unique from instance to instance
    instance_name = "my-cool-instance"

    // Security group name; this should be unique throughout your aws account
    sg_name = "outbound-ec2"

    // Set to the vpc id where the instance should be created
    vpc_id = "vpc-123456" 

    // Subnet in the above vpc where the instance will be
    subnet_id = "subnet-789abc" 

    // AMI to use; **NOTE** that these are region specific
    ami = "ami-a1b2c3d4e5f67890" 

    // The AWS ec2 instance type to sping up
    instance_type = "t3.large"

    // Some identifier for the ssh key below it; this can be anyting, but needs to be unique between ec2-instances
    public_key_name = "my-public-key"

    // The actual ssh public key
    ssh_pubkey = "ssh-rsa 1234567890 root@nasa.prod.12345" 

    // Size of root volume, in gigabytes
    ebs_volume_size = "50"

    // EBS volume type to use for the root volume
    ebs_volume_type = "gp3"

    // KMS Encryption key to use for root EBS volume encryption
    ebs_kms_key_id = "arn:aws:kms:<some-region>:123456789:key/yom0th3ri5-baa6-b1kh-33nn-xduh1231235h13t55"

    // EBS IOPS desired for root volume; can be null
    ebs_iops = null

    // Check the "Supported User Scripts" section; essentiall pick a script to run on ec2 startup; can be null for "no script".
    user_data_script_name = "code_server_tailscale_ubuntu_tls"
}
```

To test, you will need to set the following env vars on your system:
```sh
export AWS_ACCESS_KEY_ID=[Your AWS access key]
export AWS_SECRET_ACCESS_KEY=[Your AWS secret key]
export TF_VAR_tailscale_auth_key=[Your tailscale key]
```
then run some of or all of the commands below as per your needs:
```sh
# Pull terraform statefile; you must always do this once (initially) to set up terraform
$ terraform init

# Validate syntax of the module is correct; use this to check for syntax errors and typos
$ terraform validate

# Plan deployment; shows what changes will be made by applying the current state, if it is different from the 
$ terraform plan

# Apply the plan and deply the changes; you can skip this step if you are confident with the plan output
$ terraform apply
```

We recommend S3 as a backend, because it is pretty simple to set up, and because you're already using AWS. S3 seems to be the most hastle free backend to use, at least to us at the time of writing.

### Gitlab CI Configuration
Once you have tested and validated that terraform works fine, you will need to configure the following secrets in your Gitlab CI/CD variables section:
- `AWS_ACCESS_KEY_ID` - You AWS access key identifier
- `AWS_SECRET_ACCESS_KEY` - You AWS access key's associated secret key
- `TF_VAR_tailscale_auth_key` - A repeated use auth token obtained from `tailscale` which will be used to register instances with the `tailscale` network.

After everything is configured, your pipeline in `main` should set up your instances for you.

### Accessing Your Instances
#### Tailscale
If you are using `tailscale`, download their client on your favorite desktop, laptop or mobile device. Find the newly registered instance in your `tailscale` device list and copy its `tailscale` IP, and use your SSH client with the configured ssh key to access the instance.

#### Code-Server
Typically, your `code-server` url will be:
```
http://<your-instance-name>.<tailnet-domain-alias>
```
You can basically find the `tailnet-domain-alias` in your [tailnet DNS settings](https://login.tailscale.com/admin/dns). The bottom of the page should show you a domain alias that looks like `*tailsomething-lolmao123.ts.net`; everything after the `*` is the `tailnet-domain-alias`. So, if your ec2 instance name was `chasen`, then your `code-server` url would be `https://chasen.tailsomething-lolmao123.ts.net`.

If this doesn't work, you will need to find the `code-server` url by checking the good ol' log file `/var/logs/user-data.log`. If you did `code-server` with TLS, use the value for `code_server_address`, which should be shown near the end of the log file. If you did non-tls, access `code-server` on the `tailscale_ip` (in the log file, near the end) on port 8080 (i.e. `http://<tailscale_ip>:8080`).

### Supported User Scripts
You can pick a script to run when specifying the value to `user_data_script_name` above. These scripts are defined in `create_ec2_instance/scripts.cfg` folder, and run on instance startup. They can be useful for doing things like setting up gitlab runners, or installing `code-server` on an ec2-instance.

Scripts that are currently supported:
1. `code_server_tailscale_tls_ubuntu`: if you want to install code_server + `tailscale` + tls. **Note:** gitlab populates the terraform var called `tailscale_auth_key` via CI env var secrets, in order to auto install `tailscale` without requiring `SSO`. Note that `code-server` is configured with TLS using `tailscale cert`, which uses a letsencrypt cert under the hood; we feed these certs to `code-server` and everything works like magic. All the tls settings were configured in `tailscale` manually as per the guidance in the [Enabling HTTPS](https://tailscale.com/kb/1153/enabling-https/) doc.
2. `code_server_tailscale_ubuntu`: for `tailscale` + `code-server` without tls.
3. `tailscale_ubuntu`: for `tailscale` on an EC2 ubuntu machine. This pretty much gives you ssh access to the EC2 machine without having to open up port 22 on the firewall.
4. `null`: if you do not want any init scripts to be executed on your instance.

## FAQ
**Q**: Do you support adding additional EBS volumes to instances? \
**A**: No. We may in the future.

**Q**: Why the submodule? \
**A**: Cause I am too stupid to know of a better way to do this; plus, if this is a submodule, if anything breaks, you can easily make a patch request to fix it.

## License
MIT License - refer to `LICENSE` in the repository root directory.
