instances_in_region = [
  {
    instance_name = "my-cool-instance" # The name of the ec2-instance; this will be used for its hostname too, so make sure these are unique from instance to instance
    sg_name = "my-security-group" # Call this a unique security group name
    vpc_id = "vpc-123456" # Set this to the vpc id where the instance should be created
    subnet_id = "subnet-789abc" # Subnet in the above vpc where the instance will be
    ami = "ami-a1b2c3d4e5f67890" # AMI to use; note that these are region specific
    instance_type = "t3.medium" # The AWS ec2 instance type to sping up
    public_key_name = "my-public-key" # Some identifier for the ssh key below it
    ssh_pubkey = "ssh-rsa 1234567890 root@nasa.prod.12345" # The actual ssh public key
    ebs_volume_size = "50" # Size of root volume, in gigabytes
    ebs_volume_type = "gp3" # EBS volume type to use for the root volume
    ebs_kms_key_id = "arn:aws:kms:<some-region>:123456789:key/yom0th3ri5-baa6-b1th-33nn-xduh1231235h13t55" # KMS Encryption key to use for EBS volume
    ebs_iops = null # EBS IOPS desired for root volume
    user_data_script_name = "code_server_tailscale_tls_ubuntu" # Check create_ec2_instance/scripts.cfg for scripts that can be run
  }
]
