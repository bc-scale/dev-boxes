provider "aws" {
  region  = "us-east-2"
  default_tags {
    tags = local.common_tags
  }
}

terraform {
  backend "local" {
    path = "./terraform.tfstate"
  }
}

variable "instances_in_region" {
  type = list(object({
    ami                   = string
    vpc_id                = string
    subnet_id             = string
    instance_type         = string
    public_key_name       = string
    instance_name         = string
    sg_name               = string
    ssh_pubkey            = string
    ebs_volume_size       = string
    ebs_volume_type       = string
    ebs_kms_key_id        = string
    ebs_iops              = string
    user_data_script_name = string
  }))
}

variable "tailscale_auth_key" {
    type = string
}

locals {
  common_tags = {
    "repository"   = "https://gitlab.com/my/repo/location",
    "provisioning" = "gitlab-ci--project-name--terraform"
  }
}

module "create_ec2_instance" {
  source = "./dev-boxes"
  for_each = {
    for ldp in var.instances_in_region: ldp.sg_name => ldp
    # if ldp.<var> == terraform.workspace
  }
  ec2_props = each.value
  ts_auth_key = var.tailscale_auth_key
}
